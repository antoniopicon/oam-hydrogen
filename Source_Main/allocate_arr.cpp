vec3x f(nz+2, ny+2, nx+2);               f.fill(0.);   //wavefunction
vec3x fi(nz+2, ny+2, nx+2);              fi.fill(0.);  //initial wavefunction
vec3x fe(nz+2, ny+2, nx+2);              fe.fill(0.);  //excited-state wavefunction
vec1d x(nx+2);                           x.fill(0.);   //x coordinates
vec1d y(ny+2);                           y.fill(0.);   //y coordinates
vec1d z(nz+2);                           z.fill(0.);   //z coordinates
vec1d r(nr+2);                           r.fill(0.);   //r spherical coordinates

vec1x bx(nx+2);                          bx.fill(0.);  //for tridiagonal algorithm
vec1x by(ny+2);                          by.fill(0.);  //for tridiagonal algorithm
vec1x bz(nz+2);                          bz.fill(0.);  //for tridiagonal algorithm
vec1x rx(nx+2);                          rx.fill(0.);  //for tridiagonal algorithm
vec1x ry(ny+2);                          ry.fill(0.);  //for tridiagonal algorithm
vec1x rz(nz+2);                          rz.fill(0.);  //for tridiagonal algorithm

vec1d fabso(nabs+2);                     fabso.fill(0.); //for absorption in the boundaries
vec1i f_indf(23);                        f_indf.fill(0.);

//arrays for vector potential
vec3d c_ay(nz+2,ny+2,nx+2);  c_ay.fill(0.);
vec3d c_az(nz+2,ny+2,nx+2);  c_az.fill(0.);

vec1x ay(ny+2);              ay.fill(0.);
vec1x az(nz+2);              az.fill(0.);
vec1x cy(ny+2);              cy.fill(0.);
vec1x cz(nz+2);              cz.fill(0.);

