ifstream fp_input;
if ( argc != 2 )
{
    cout<<"usage: "<< argv[0] <<" <filename>" << endl;
}
else
{
    fp_input.open(argv[1]);
    if (!fp_input.is_open())
    {
        cout << "error opening file " << argv[1] << endl;
    }
}
    
printf("Starting read input...\n");

Read_Input(fp_input,param);
    
printf("End read input...\n");
    fp_input.close();
