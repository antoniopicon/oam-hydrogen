norma=normae=vespx=vespy=vespz=pb5n=pb10n=pb0=pb5p=pb10p=0.;
lx=ly=lz=lxe=lye=lze=0.;
linvyx=linvyz=linvzx=linvzy=linvyxe=linvyze=linvzxe=linvzye=0.;
for(int iz=1;iz<=nz;iz++){
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            mod=real(f[iz][iy][ix]*conj(f[iz][iy][ix]));
            mode=real(fe[iz][iy][ix]*conj(fe[iz][iy][ix]));
            norma+=mod;
            normae+=mode;
            vespx+=x[ix]*mod;
            vespy+=y[iy]*mod;
            vespz+=z[iz]*mod;
            lx+=(y[iy]*(f[iz+1][iy][ix]-f[iz-1][iy][ix])/(2.*dz)- z[iz]*(f[iz][iy+1][ix]-f[iz][iy-1][ix])/(2.*dy))*conj(f[iz][iy][ix]);
            ly+=(z[iz]*(f[iz][iy][ix+1]-f[iz][iy][ix-1])/(2.*dx)- x[ix]*(f[iz+1][iy][ix]-f[iz-1][iy][ix])/(2.*dz))*conj(f[iz][iy][ix]);
            lz+=(x[ix]*(f[iz][iy+1][ix]-f[iz][iy-1][ix])/(2.*dy)- y[iy]*(f[iz][iy][ix+1]-f[iz][iy][ix-1])/(2.*dx))*conj(f[iz][iy][ix]);
            lxe+=(y[iy]*(fe[iz+1][iy][ix]-fe[iz-1][iy][ix])/(2.*dz)- z[iz]*(fe[iz][iy+1][ix]-fe[iz][iy-1][ix])/(2.*dy))*conj(fe[iz][iy][ix]);
            lye+=(z[iz]*(fe[iz][iy][ix+1]-fe[iz][iy][ix-1])/(2.*dx)- x[ix]*(fe[iz+1][iy][ix]-fe[iz-1][iy][ix])/(2.*dz))*conj(fe[iz][iy][ix]);
            lze+=(x[ix]*(fe[iz][iy+1][ix]-fe[iz][iy-1][ix])/(2.*dy)- y[iy]*(fe[iz][iy][ix+1]-fe[iz][iy][ix-1])/(2.*dx))*conj(fe[iz][iy][ix]);
            
            linvyx+=mod*c_ay[iz][iy][ix]*x[ix];
            linvyz+=mod*c_ay[iz][iy][ix]*z[iz];
            linvzx+=mod*c_az[iz][iy][ix]*x[ix];
            linvzy+=mod*c_az[iz][iy][ix]*y[iy];
            linvyxe+=mode*c_ay[iz][iy][ix]*x[ix];
            linvyze+=mode*c_ay[iz][iy][ix]*z[iz];
            linvzxe+=mode*c_az[iz][iy][ix]*x[ix];
            linvzye+=mode*c_az[iz][iy][ix]*y[iy];
            
            if(x[ix]<-10.) pb10n+=mod;
            if(x[ix]<-5.) pb5n+=mod;
            if(x[ix]<0.) pb0+=mod;
            if(x[ix]<=5.) pb5p+=mod;
            if(x[ix]<=10.) pb10p+=mod;
        }
    }
}

fp1 << setw(25) << setprecision(15) << time/t1c << " " << norma << " " << real(alpha*conj(alpha)) << " " << normae << endl;
fp2 << setw(25) << setprecision(15) << time/t1c << " " << vespx << " " << vespy << " " << vespz << endl;
fp3 << setw(25) << setprecision(15) << time/t1c << " " << imag(lx) << " " << imag(ly) << " " << imag(lz) << endl;
fp3_gauge << setw(25) << setprecision(15) << time/t1c << " " << imag(lx)-linvyz+linvzy << " " << imag(ly)-linvzx << " " << imag(lz)+linvyx << endl;
fp3_gauge_e << setw(25) << setprecision(15) << time/t1c << " " << imag(lxe)-linvyze+linvzye << " " << imag(lye)-linvzxe << " " << imag(lze)+linvyxe << endl;
fp_pb_x << setw(25) << setprecision(15) << time/t1c << " " << pb0-pb5n << " " << pb5p-pb0 << " " << pb10p-pb5p << endl;

//fprintf(fp1,"%le %le %le %le\n",time/t1c,norma,real(alpha*conj(alpha)),normae);
//fprintf(fp2,"%le %le %le %le\n",time/t1c,vespx,vespy,vespz);
//fprintf(fp3,"%le %le %le %le\n",time/t1c,imag(lx),imag(ly),imag(lz));
//fprintf(fp3_gauge,"%le %le %le %le\n",time/t1c,imag(lx)-linvyz+linvzy,imag(ly)-linvzx,imag(lz)+linvyx); //Angular momentum
//fprintf(fp3_gauge_e,"%le %le %le %le\n",time/t1c,imag(lxe)-linvyze+linvzye,imag(lye)-linvzxe,imag(lze)+linvyxe);
//fprintf(fp_pb_x,"%le %le %le %le\n",time/t1c,pb0-pb5n,pb5p-pb0,pb10p-pb5p);


//print wavefunction as cube file
string label = "wf";
//Print_vec3x(param,fe,label,it);
