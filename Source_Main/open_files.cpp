ofstream fp1,fp2,fp3,fp3_gauge,fp3_gauge_e;
ofstream fp_bordes,fp_pb_x;

ofstream fp_wf;

fp1.open("Output/LG_nrh3D1s_2cnd.txt"); //print time/t1c,norma,real(alpha*conj(alpha)),normae  (norms)
fp2.open("Output/LG_vph3D1s_2cnd.txt"); //print time/t1c,vespx,vespy,vespz                      (positions)
fp3.open("Output/LG_lnh3D1s_2cnd.txt"); //print time/t1c,imag(lx),imag(ly),imag(lz))              (angular momenta)
fp3_gauge.open("Output/LG_lih3D1s_2cnd.txt"); // OAM
fp3_gauge_e.open("Output/LG_lih3D1s_2cnd_e.txt"); //OAM excited state
fp_bordes.open("Output/LG_bordes.txt"); // print time/t1c,escx,escy,escz                          (population in the boundaries)
fp_pb_x.open("Output/LG_poblacion_x.txt"); // print time/t1c,pb0-pb5n,pb5p-pb0,pb10p-pb5p      (population distributed in x)
