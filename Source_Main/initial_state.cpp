norma=0.;

FILE * fp_initial;
fp_initial=fopen("f3D_010101","r");

if(fp_initial) cout << "Reading initial population..." << endl;
else {
    cout << "Not finding file of initial population" << endl;
    return 0;
}

for(int iz=1;iz<=npw;iz++){
    for(int iy=1;iy<=npw;iy++){
        for(int ix=1;ix<=npwx;ix++){
            fscanf(fp_initial,"%le\n",&valor);
            f[midz-npw+iz][midy-npw+iy][midx-npwx+ix]=complexd(valor,0.);
            fi[midz-npw+iz][midy-npw+iy][midx-npwx+ix]=complexd(valor,0.);
            f[midz-npw+iz][midy-npw+iy][midx+npwx+1-ix]=complexd(valor,0.);
            fi[midz-npw+iz][midy-npw+iy][midx+npwx+1-ix]=complexd(valor,0.);
            f[midz-npw+iz][midy+npw+1-iy][midx-npwx+ix]=complexd(valor,0.);
            fi[midz-npw+iz][midy+npw+1-iy][midx-npwx+ix]=complexd(valor,0.);
            f[midz-npw+iz][midy+npw+1-iy][midx+npwx+1-ix]=complexd(valor,0.);
            fi[midz-npw+iz][midy+npw+1-iy][midx+npwx+1-ix]=complexd(valor,0.);
            f[midz+npw+1-iz][midy-npw+iy][midx-npwx+ix]=complexd(valor,0.);
            fi[midz+npw+1-iz][midy-npw+iy][midx-npwx+ix]=complexd(valor,0.);
            f[midz+npw+1-iz][midy-npw+iy][midx+npwx+1-ix]=complexd(valor,0.);
            fi[midz+npw+1-iz][midy-npw+iy][midx+npwx+1-ix]=complexd(valor,0.);
            f[midz+npw+1-iz][midy+npw+1-iy][midx-npwx+ix]=complexd(valor,0.);
            fi[midz+npw+1-iz][midy+npw+1-iy][midx-npwx+ix]=complexd(valor,0.);
            f[midz+npw+1-iz][midy+npw+1-iy][midx+npwx+1-ix]=complexd(valor,0.);
            fi[midz+npw+1-iz][midy+npw+1-iy][midx+npwx+1-ix]=complexd(valor,0.);
            norma+=valor*valor;
        }
    }
}
fclose(fp_initial);

norma=sqrt(8.*norma);
for(int iz=1;iz<=nz;iz++){
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            f[iz][iy][ix]/=norma;
            fi[iz][iy][ix]/=norma;
        }
    }
}
