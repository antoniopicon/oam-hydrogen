//spatial vectors
for(int ix=0;ix<=nx+1;ix++) x[ix]=(ix-midx-.5)*dx;
for(int iy=0;iy<=ny+1;iy++) y[iy]=(iy-midy-.5)*dy;
for(int iz=0;iz<=nz+1;iz++) z[iz]=(iz-midz-.5)*dz;
for(int ir=0;ir<=nr+1;ir++) r[ir]=(ir-.5)*dr;

//absorbing boundaries
for(int ix=0;ix<nabs;ix++) fabso[ix]=pow(sin(pi*ix/(2.*(nabs+10))),2.);


//useful definitions
dt2=dt/2.;
dt4=dt/4.;
dx2=1./dx/dx;
dy2=1./dy/dy;
dz2=1./dz/dz;
auxdx=c1*dt/(8.*dx*dx);
auxdz=c1*dt/(8.*dz*dz);
auxdy=c1*dt/(4.*dy*dy);
auxty=dt/(4.*dy);
auxtz=dt/(8.*dz);
ax=cx=-auxdx;
// az=cz=-auxdz;

auxz1=dt4*dz2;
auxz2=dt4/3.;
auxx1=dt4*dx2;
auxx2=dt4/2.;
auxx3=dt4/3.;
auxy1=dt2*dy2;
auxy2=dt2/3.;
