
double time=it*dt;

double xp0=time*c;
//double xp1=xp0-lambda;
double xp2=xp0-nctot*lambda;

//defining vector potential
for(int iz=1;iz<=nz;iz++){
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            fase=x[ix]-c*time;
            if(x[ix]>xp0||x[ix]<=xp2) envelopet=0.;
            else envelopet=sin(kenv*fase)*sin(kenv*fase);
            envelope=LG(l,p,z0,k,x[ix],y[iy],z[iz]);
            c_ay[iz][iy][ix]=e0*envelopet*real(exp(c1*k*fase)*envelope)*(2);
                
            //c_ez=e0*envelopet*cos(k*fase);
            //c_az[iz][iy][ix]-=dt*c_ez;
        }
    }
}

// direccion z
for(int iy=1;iy<=ny;iy++){
    for(int ix=1;ix<=nx;ix++){
        double radxy=x[ix]*x[ix]+y[iy]*y[iy];
        for(int iz=1;iz<=nz;iz++){
            az[iz]=-auxtz*(c_az[iz-1][iy][ix]+c_az[iz][iy][ix])/2.-auxdz;
            cz[iz]=auxtz*(c_az[iz+1][iy][ix]+c_az[iz][iy][ix])/2.-auxdz;
            double v=-1./sqrt(z[iz]*z[iz]+radxy);
            bz[iz]=complexd(1.,auxz1+auxz2*v);
            rz[iz]=(2.-bz[iz])*f[iz][iy][ix]-az[iz]*f[iz-1][iy][ix]-cz[iz]*f[iz+1][iy][ix];
        }
        tridz(az,bz,cz,rz,f,ix,iy);
    }
}

// direccion x
for(int iz=nz;iz>=1;iz--){
    for(int iy=ny;iy>=1;iy--){
        double radxy=z[iz]*z[iz]+y[iy]*y[iy];
        for(int ix=1;ix<=nx;ix++){
            double v=-1./sqrt(x[ix]*x[ix]+radxy);
            bx[ix]=1.+c1*(auxx1+auxx2*c_ay[iz][iy][ix]*c_ay[iz][iy][ix]+ auxx2*c_az[iz][iy][ix]*c_az[iz][iy][ix]+auxx3*v);
            rx[ix]=(2.-bx[ix])*f[iz][iy][ix]-ax*f[iz][iy][ix-1]-cx*f[iz][iy][ix+1];
        }
        tridx(ax,bx,cx,rx,f,iy,iz);
    }
}

// direccion y
for(int iz=1;iz<=nz;iz++){
    for(int ix=1;ix<=nx;ix++){
        double radxy=z[iz]*z[iz]+x[ix]*x[ix];
        for(int iy=1;iy<=ny;iy++){
            ay[iy]=-auxty*(c_ay[iz][iy-1][ix]+c_ay[iz][iy][ix])/2.-auxdy;
            cy[iy]=auxty*(c_ay[iz][iy+1][ix]+c_ay[iz][iy][ix])/2.-auxdy;
            double v=-1./sqrt(y[iy]*y[iy]+radxy);
            by[iy]=complexd(1.,auxy1+auxy2*v);
            ry[iy]=(2.-by[iy])*f[iz][iy][ix]-ay[iy]*f[iz][iy-1][ix]-cy[iy]*f[iz][iy+1][ix];
        }
        tridy(ay,by,cy,ry,f,ix,iz);
    }
}

// direccion x
for(int iz=nz;iz>=1;iz--){
    for(int iy=ny;iy>=1;iy--){
        double radxy=z[iz]*z[iz]+y[iy]*y[iy];
        for(int ix=1;ix<=nx;ix++){
            double v=-1./sqrt(x[ix]*x[ix]+radxy);
            bx[ix]=1.+c1*(auxx1+auxx2*c_ay[iz][iy][ix]*c_ay[iz][iy][ix]+ auxx2*c_az[iz][iy][ix]*c_az[iz][iy][ix]+auxx3*v);
            rx[ix]=(2.-bx[ix])*f[iz][iy][ix]-ax*f[iz][iy][ix-1] -cx*f[iz][iy][ix+1];
        }
        tridx(ax,bx,cx,rx,f,iy,iz);
    }
}

// direccion z
for(int iy=1;iy<=ny;iy++){
    for(int ix=1;ix<=nx;ix++){
        double radxy=x[ix]*x[ix]+y[iy]*y[iy];
        for(int iz=1;iz<=nz;iz++){
            az[iz]=-auxtz*(c_az[iz-1][iy][ix]+c_az[iz][iy][ix])/2.-auxdz;
            cz[iz]=auxtz*(c_az[iz+1][iy][ix]+c_az[iz][iy][ix])/2.-auxdz;
            double v=-1./sqrt(z[iz]*z[iz]+radxy);
            bz[iz]=complexd(1.,auxz1+auxz2*v);
            rz[iz]=(2.-bz[iz])*f[iz][iy][ix]-az[iz]*f[iz-1][iy][ix]-cz[iz]*f[iz+1][iy][ix];
        }
        tridz(az,bz,cz,rz,f,ix,iy);
    }
}

// excited state
alpha=0.;
for(int iz=1;iz<=nz;iz++){
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            alpha+=fi[iz][iy][ix]*f[iz][iy][ix];
            fe[iz][iy][ix]=0.;
        }
    }
}
for(int iz=1;iz<=nz;iz++){
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            fe[iz][iy][ix]=f[iz][iy][ix]-(alpha*fi[iz][iy][ix]);
        }
    }
}

// absorption in the boundaries
indps++;
if(indps==indws){
    indps=0;
    escy=0.;
    for(int iz=1;iz<=nz;iz++){
        for(int ix=1;ix<=nx;ix++){
            for(int iy=0;iy<nabs;iy++){
                f[iz][iy][ix]*=fabso[iy];
                f[iz][ny+1-iy][ix]*=fabso[iy];
                escy+=real(f[iz][iy][ix]*conj(f[iz][iy][ix]))+ real(f[iz][ny+1-iy][ix]*conj(f[iz][ny+1-iy][ix]));
            }
        }
    }
    escx=0.;
    for(int iz=1;iz<=nz;iz++){
        for(int iy=1;iy<=ny;iy++){
            for(int ix=0;ix<nabs;ix++){
                f[iz][iy][ix]*=fabso[ix];
                f[iz][iy][nx+1-ix]*=fabso[ix];
                escx+=real(f[iz][iy][ix]*conj(f[iz][iy][ix]))+ real(f[iz][iy][nx+1-ix]*conj(f[iz][iy][nx+1-ix]));
            }
        }
    }
    escz=0.;
    for(int iy=1;iy<=ny;iy++){
        for(int ix=1;ix<=nx;ix++){
            for(int iz=0;iz<nabs;iz++){
                f[iz][iy][ix]*=fabso[iz];
                f[nz+1-iz][iy][ix]*=fabso[iz];
                escz+=real(f[iz][iy][ix]*conj(f[iz][iy][ix]))+ real(f[nz+1-iz][iy][ix]*conj(f[nz+1-iz][iy][ix]));
            }
        }
    }
    fp_bordes << setw(25) << setprecision(15) << time/t1c << " " << escx << " " << escy << " " << escz << endl;
    //fprintf(fp_bordes,"%le %le %le %le\n",time/t1c,escx,escy,escz);
}

