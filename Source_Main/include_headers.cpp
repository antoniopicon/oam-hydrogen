#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <cmath>
#include <complex>
#include <ctime>
//#include <armadillo>
#include <memory>
#include <vector>

using namespace std;


#include "BoostArrays.h"
#include "Constants.h"
#include "typedef.h"
#include "tridiagonal.h"
#include "Interpolation.h"
#include "beams.h"
#include "Observables.h"
#include "read_input.h"

