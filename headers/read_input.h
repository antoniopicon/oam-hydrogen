void Separate_string(string& s, vector<string>& str, size_t& pos)
{
  str.clear();
        s = s.substr(0,pos); //cout << s << endl;
        str.push_back("");
        size_t count = 0;
        size_t w=0;  
        while(w != pos)            
        {
            if( s[w] != ' ')
            {
                str[count] += s[w];
            }
            else
            {
              if( 0 != strcasecmp(str[count].c_str(), "") )
              {
                count++;
                str.push_back("");
              }
            }
            w++;
        }
        if(str.back()=="")
          str.pop_back();
    
}



bool blank(string& s)
{
  size_t pos = s.length();
  size_t w = 0;
  bool blank = true;
  while(w!=pos) 
  {
      if(s[w]!= ' ')
      {
           blank = false;
           break;
      }
      w++;
  }
  return blank;
}

void End_Section(ifstream& fp_input, string str)
{
  string s;
  while((getline(fp_input,s)))
  {
   // cout << s << endl;

    if(0 == strcasecmp(s.c_str(), "")){}
    else
    {
          
          if(!blank(s))
          {
              size_t pos = s.find("}");
              if(pos == -1)
              { 
                  printf("Cannot find end of %s. Correct your input.\n", str.c_str());
                  exit(1);
              }
              else
              {
                 // printf("%s section read.\n", str.c_str());
                  return;
              } 
          }
    } 
  }
}




void Read_Input (ifstream& fp_input, PortableIn& param)
{
        string s;

        while(getline(fp_input,s))
        {
           // cout << s << endl;
           if(s!="")
           {
              vector<string> str;
              size_t pos = s.find("{");
            


              if (pos == -1)
              {
                 printf("Can't find the character { in the string \" %s \". \nPlease, review your input.\n", s.c_str());
                 exit(1);
              }


              else  Separate_string(s, str, pos);
               
               
               if(0 == strcasecmp(str[0].c_str(), "grid"))
               {
                   s="";
                   while(s==""){getline(fp_input, s);}
                   
                   while(s.find("}") == -1 || 0 == strcasecmp( s.c_str(), ""))
                   {
                       str.clear();
                       pos = s.length();
                       Separate_string(s, str, pos);
                       if(strcasecmp(str[0].c_str(), "dx")==0)
                           param.dx = atof(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "dy")==0)
                           param.dy = atof(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "dz")==0)
                           param.dz = atof(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "nx")==0)
                           param.NX = atoi(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "ny")==0)
                           param.NY = atoi(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "nz")==0)
                           param.NZ = atoi(str[1].c_str());
                       else if(!blank(str[0]) || (0 != strcasecmp(str[0].c_str(), "")))
                       {
                         printf("Can't recognize grid type %s. Possible types: dx, dy, dz, nx, ny, nz.\n", str[0].c_str());
                         exit(1);
                       }
                       if(!(getline(fp_input,s)))
                       {
                           printf("Can't find the end of bands section.\n");
                           exit(1);
                       }
                   }
               }

               else if(0 == strcasecmp(str[0].c_str(), "laser"))
               {
                   s="";
                   while(s==""){getline(fp_input, s);}
                   
                   while(s.find("}") == -1 || 0 == strcasecmp( s.c_str(), ""))
                   {
                       str.clear();
                       pos = s.length();
                       Separate_string(s, str, pos);
                       if(strcasecmp(str[0].c_str(), "intensity")==0)
                           param.intensity = atof(str[1].c_str());
                       else if (strcasecmp(str[0].c_str(), "frequency")==0)
                           param.freq = atof(str[1].c_str());
                       else if(!blank(str[0]) || (0 != strcasecmp(str[0].c_str(), "")))
                       {
                         printf("Can't recognize grid type %s. Possible types: intensity, frequency.\n", str[0].c_str());
                         exit(1);
                       }
                       if(!(getline(fp_input,s)))
                       {
                           printf("Can't find the end of bands section.\n");
                           exit(1);
                       }
                   }
                   
               }
               
               
              


           }//end of if       
         }//end of while

}



