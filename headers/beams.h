/* ********************************************************
            Laguerre-Gauss function
******************************************************** */

complexd LG(int l, int p, double z0, double k, double x, double y, double z) {

    double ra, w0, w, R, arg, argpot, phi, intphi, gouy;
    double ptot, pot, comL, com2L, com3L, com4L, L, fasePhi, yr, zr;
    int m, r, s, t;
    complexd valor;

    w0=sqrt(2*z0/k);
    ra=y*y+z*z;
    w=w0*sqrt(1+(x/z0)*(x/z0));
    R=x+(z0*z0)/x;
    arg=2*ra/w/w;
    argpot=ptot=1;
    comL=1;
    L=0;
    phi = 0;
    gouy = 0;
    
    //printf("%e\n",fabs(l)+p);
    if(fabs(l)+p==0) comL=1; else for(m=1;m<=(fabs(l)+p);m++) comL = comL*m;
    if(p==0) ptot=1; else for(m=1;m<=p;m++) ptot=ptot*m;

    for (m=0;m<=p;m++){
        pot=com2L=com3L=com4L=1;
        if(fabs(l)+m==0) com2L=1; else for(r=1;r<=(fabs(l)+m);r++) com2L = com2L*r;
        if(p-m==0) com3L=1; else for (s=1;s<=p-m;s++) com3L = com3L*s;
        if(m==0) com4L=1; else for(r=1;r<=m;r++) com4L=com4L*r;
        if(m==0) pot=1; else for(t=1;t<=m;t++) pot=pot*arg*(-1);
        L+=pot*comL/com2L/com3L/com4L;
        }
    if(fabs(l)==0) argpot=1; else for(m=1;m<=fabs(l);m++) argpot=argpot*sqrt(arg);
//defino y,z rotadas un cierto ángulo, por si quiero cambiar el sistema de referencia de phi.
    yr=y*cos(0)-z*sin(0);
    zr=y*sin(0)+z*cos(0);
    if(yr>=0. && zr>=0.){
        phi=atan(zr/yr); phi=l*phi/2/pi; phi=modf(phi,&intphi); phi=2*pi*phi;}
    else if(yr>=0. && zr<0.){
        phi=2.*pi-atan(fabs(zr)/yr); phi=l*phi/2/pi; phi=modf(phi,&intphi); phi=2*pi*phi;}
    else if(yr<0. && zr<0.){
        phi=pi+atan(fabs(zr)/fabs(yr)); phi=l*phi/2/pi; phi=modf(phi,&intphi); phi=2*pi*phi;}
    else if(yr<0. && zr>=0.){
        phi=pi-atan(zr/fabs(yr)); phi=l*phi/2/pi; phi=modf(phi,&intphi); phi=2*pi*phi;}
    if(x>=0. && z0>=0.){
        gouy=(2*p +fabs(l)+1)*atan(x/z0);}
    else if(x<0. && z0>=0.){
        gouy=(2*p +fabs(l)+1)*(2.*pi-atan(fabs(x)/z0));}
    fasePhi= phi + k*ra/2/R - gouy;
    valor= sqrt(2*ptot/pi/comL)*argpot*L*exp(-arg/2)*cos(fasePhi)/w +
        c1*sqrt(2*ptot/pi/comL)*argpot*L*exp(-arg/2)*sin(fasePhi)/w;
    return(valor);
}
