/* ******************************************************
            resolucion de la tridiagonal en x
****************************************************** */

void tridx(complexd a,vec1x& b,complexd c,vec1x& r,vec3x& f,int iy,int iz){
complexd bet;
vec1x gam(nxp2); gam.fill(0.);
bet=b[1];
f[iz][iy][1]=r[1]/bet;
for(int j=2;j<=nx;j++){
    gam[j]=c/bet;
    bet=b[j]-a*gam[j];
    f[iz][iy][j]=(r[j]-a*f[iz][iy][j-1])/bet;
}
for(int j=nx-1;j>=1;j--)
    f[iz][iy][j]-=gam[j+1]*f[iz][iy][j+1];
    
}


/* ********************************************************
           resolucion de la tridiagonal en y
******************************************************** */

void tridy(vec1x& a,vec1x& b,vec1x& c,vec1x& r,vec3x& f,int ix,int iz){
complexd bet;
vec1x gam(ny+2); gam.fill(0.);
bet=b[1];
f[iz][1][ix]=r[1]/bet;
for(int j=2;j<=ny;j++){
    gam[j]=c[j-1]/bet;
    bet=b[j]-a[j]*gam[j];
    f[iz][j][ix]=(r[j]-a[j]*f[iz][j-1][ix])/bet;
}
for(int j=ny-1;j>=1;j--)
    f[iz][j][ix]-=gam[j+1]*f[iz][j+1][ix];

}

/* ********************************************************
           resolucion de la tridiagonal en z
******************************************************** */

void tridz(vec1x& a,vec1x& b,vec1x& c,vec1x& r,vec3x& f,int ix,int iy) {
complexd bet;
vec1x gam(nz+2); gam.fill(0.);
bet=b[1];
f[1][iy][ix]=r[1]/bet;
for(int j=2;j<=nz;j++){
    gam[j]=c[j-1]/bet;
    bet=b[j]-a[j]*gam[j];
    f[j][iy][ix]=(r[j]-a[j]*f[j-1][iy][ix])/bet;
}
for(int j=nz-1;j>=1;j--)
    f[j][iy][ix]-=gam[j+1]*f[j+1][iy][ix];

}
