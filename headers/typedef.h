typedef complex<double>                           complexd;
typedef multivec1D<int>                           vec1i;
typedef multivec2D<int>                           vec2i;
typedef multivec3D<int>                           vec3i;
typedef multivec4D<int>                           vec4i;
typedef multivec5D<int>                           vec5i;
typedef multivec6D<int>                           vec6i;
typedef multivec1D<double>                        vec1d;
typedef multivec2D<double>                        vec2d;
typedef multivec3D<double>                        vec3d;
typedef multivec4D<double>                        vec4d;
typedef multivec5D<double>                        vec5d;
typedef multivec6D<double>                        vec6d;
typedef multivec1D<complexd>                      vec1x;
typedef multivec2D<complexd>                      vec2x;
typedef multivec3D<complexd>                      vec3x;
typedef multivec4D<complexd>                      vec4x;
typedef multivec5D<complexd>                      vec5x;
typedef multivec6D<complexd>                      vec6x;

vec1i    vecE1i; 
vec2i    vecE2i;
vec3i    vecE3i;
vec4i    vecE4i;
vec5i    vecE5i;
vec6i    vecE6i;
vec1d    vecE1d;
vec2d    vecE2d;
vec3d    vecE3d;
vec4d    vecE4d;
vec5d    vecE5d;
vec6d    vecE6d;
vec1x    vecE1x;
vec2x    vecE2x;
vec3x    vecE3x;
vec4x    vecE4x;
vec5x    vecE5x;
vec6x    vecE6x;

class PortableIn
{
public:
    //grid
    int    NX;
    int    NY;
    int    NZ;
    double dx;
    double dy;
    double dz;
    
    //laser
    double intensity;
    double freq;
    
};
