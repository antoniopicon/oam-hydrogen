void Print_vec3x(PortableIn& param, vec3x& f, string& label, int it){
    string name_file;
    stringstream sname;
    sname.seekp(0,ios::beg);
    sname <<  it;
    
    name_file= "Output/" + label + "_" + sname.str() + ".cube";
    cout<< name_file << endl;
    ofstream fp_ED; fp_ED.open(name_file.c_str());
    
    vec1d spacings(3);
    spacings[0]=param.dx;
    spacings[1]=param.dy;
    spacings[2]=param.dz;
    vec1d Origin(3);
    Origin[0]=-int(nx/2)*param.dx;
    Origin[1]=-int(ny/2)*param.dy;
    Origin[2]=-int(nz/2)*param.dz;
    
    vec3d Probf(nx,ny,nz); Probf.fill(0.);
    for(int iz=1;iz<=nz;iz++){
        for(int iy=1;iy<=ny;iy++){
            for(int ix=1;ix<=nx;ix++){
                Probf[iz][iy][ix]=real(f[iz][iy][ix]*conj(f[iz][iy][ix]));
            }
        }
    }
    FittedData<double> MO(Probf,spacings,Origin);
    
    //int NX = nx;
    //int NY = ny;
    //int NZ = nz;
    int NX=41;
    int NY=41;
    int NZ=52;
    double DX=0.2;
    double DY,DZ; DY=DX; DZ=DX;
    double Offset=0.1;
    
    fp_ED.precision(6); fp_ED.setf( std::ios::fixed, std::ios::floatfield );
    fp_ED << "Cube data\nMolecular Orbital\n";
    fp_ED << 1 << "    " << -DX*(NX/2.) << "    " << -DY*(NY/2.) << "    " << -DZ*(NZ/2.)+Offset << "\n";
    fp_ED << NX << "    " << DX << "    " << "0.000000" << "    " << "0.000000\n";
    fp_ED << NY << "    " << "0.000000" << "    " << DY << "    " << "0.000000\n";
    fp_ED << NZ << "    " << "0.000000" << "    " << "0.000000" << "    " << DZ << "\n";
    
    fp_ED << "1" << "    " << "H" << "    " << "0.000000" << "    " << "0.000000" << "    " << "0.000000" << endl;
    
    //fp_ED << int(Atom_xyz(i,3)) << "    " << Atom_xyz(i,3) << "    " << Atom_xyz(i,0) << "    " << Atom_xyz(i,1) << "    " << Atom_xyz(i,2) << endl;
    
    fp_ED.precision(5); fp_ED.setf( std::ios::scientific, std::ios::floatfield );
    
    for (int ix=0;ix<NX;ix++) {
        double xx=(ix-int(NX/2))*DX;
        for (int iy=0;iy<NY;iy++) {
            double yy=(iy-int(NY/2))*DY;
            for (int iz=0;iz<NZ;iz++) {
                double zz=(iz-int(NZ/2))*DZ+Offset;
                fp_ED << MO(xx,yy,zz) << " ";
                if (iz % 6 == 5)
                    fp_ED << "\n"; //printf("\n");
            }
            fp_ED << "\n"; //printf("\n");
        }
    }
    fp_ED.close();
}
