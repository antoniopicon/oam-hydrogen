// shiva: g++ -lm -lcomplex

//*****************************************
// HIDROGENO TRIDIMENSIONAL NO DIPOLAR
// POLARIZACION LINEAL (se puede extender a polarizaci�n circular) y MODO TRANSVERSAL LAGUERRE-GAUSS.
// Cuidado I: El pulso EM se le puede introducir un shift, para que justo cuando empiece el programa, el pulso
// no se encuentre justo interaccionando con el �tomo.
// Cuidado II: El origen de la fase azimuthal del LG se puede modificar, ver la definici�n de LG para yr y zr (variables rotadas,
// para cambiar el origen de phi). 
// La proyecci�n en el plano zy se pinta de tal forma que escogiendo el sistema de referencia espacial t�pico xyz, y suponiendo
// que el haz se propaga en x, la proyecci�n del haz en el plano transveral es mirando el haz que llega desde x.

// 1. Se ha ampliado las proyecciones en harmonicos esf�ricos, hasta el harm�nico g (L=4).
// 2. El haz ahora no es un pulso, empieza creciendo como un seno cuadrado (en un ciclo del l�ser) y despu�s es constante hasta el final. 
// 3. Se incorpora la absorbente (el archivo se renombra: "LG_sch3D1s_2cnd.txt" a "LG_bordes.txt").
// 4. El archivo de las poblaciones en el eje x tambi�n se modifica, renombr�ndolo: "LG_psim3D1s_2cnd.txt" a "LG_poblacion_x.txt").
// 5. Se modifica el potencial vector, anteriormente se definia a partir del campo el�ctrico, ahora se incorpora el potencial vector directamente.
//*****************************************

#define nx 250
#define ny 400
#define nz 400
#define nxp2 252
#define nyp2 402
#define nxyp2 252*402
#define midx nx/2
#define midy ny/2
#define midz nz/2
#define nabs 30  //points of absorption in the boundaries
#define npw 200  //grid for initial state
#define npwx 125 //grid for initial state in x direction
#define nr 51    //grid for r spherical coordinates
#define dr 0.4

#include "Source_Main/include_headers.cpp"

int main(int argc, char* argv[]){
    
    

//define some variables
    #include "Source_Main/variables.cpp"
//read the input file
    #include "Source_Main/input.cpp"
    dx=param.dx;
    dy=param.dy;
    dz=param.dz;
//allocate large arrays
	#include "Source_Main/allocate_arr.cpp"

//pulse parameters
	e0=1.e+10/8.;
	wl=1.;
	a0=e0/wl;
	elipt=1.;
	
	nctot=3;
	t1c=2.*pi/wl;
	dt=t1c/200.;
	nit=200*nctot;
	lambda=2.*pi*c/wl;
	k=2.*pi/lambda;
	wenv=pi/(t1c*nctot);
	kenv=pi/(nctot*lambda);
	
	indw=4;
	indp=indw-1;
	indws=24;      //which frequency the absorption in the boundary is applied
	indps=indws-1;

//defining grid
#include "Source_Main/grid.cpp"
    
    
//reading initial state
#include "Source_Main/initial_state.cpp"
// ficheros de salida
    system("mkdir Output");
    #include "Source_Main/open_files.cpp"
    
	i_indf=0;

// para LG
	l=1;
	p=0;
	z0=30000000;
    
//  temporal evolution
    for(int it=0;it<=100;it++){
        
    //for(int it=0;it<=nit;it++){
        #include "Source_Main/timepropagation.cpp"
        
        indp++;
        if(indp==indw){
            indp=0;
            #include "Source_Main/tdprint.cpp"
        }
    }

    fp1.close();
    fp2.close();
    fp3.close();
    fp3_gauge.close();
    fp3_gauge_e.close();
    fp_bordes.close();
    fp_pb_x.close();

    


    return 0;
}


// final del programa ---------------
