CXX = clang++
#CXXFLAGS = -Wshadow
CXXFLAGS = -ansi -Wall -W -g -Wwrite-strings -std=c++14 -O3 -ffast-math #-framework Accelerate #-fopenmp -lstdc++
LFLAGS = -L/opt/local/lib
INCLUDES = -I./headers/ -I/opt/local/include/ 
LIBS = -lm -lgsl #-larmadillo
NAMEXE = OAM_Hydrogen.out
OBJS = OAM_Hydrogen.o

.PHONY: clean all 

all: ending clean
	@echo All programs compiled

ending: $(OBJS)
	$(CXX) $(CXXFLAGS) $(LFLAGS) $(OBJS) $(INCLUDES) $(LIBS) -o $(NAMEXE)
	@echo Linked

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $<
	@echo Compiled
	
clean: 
	$(RM) $(OBJS)
# DO NOT DELETE
